/**
 * Buffer
 * 
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2018
 */
package exercicio4;

public interface Buffer {

    public void set (int value, String taskName )throws InterruptedException;
    
    public int get ( String taskName ) throws InterruptedException;
    
}
