/**
 * Exemplo de BufferSincronizado com métodos
 * wait(), notify() e notifyAll()
 * 
 * Autor: Paulo Henrique Bordignon
 * Ultima modificacao: 31/03/2018
 */
package exercicio4;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
/**
 *
 * @author Paulo
 */
public class BufferSincronizado implements Buffer {

    //Tempo aleatorio em que cada thread entra em sleep
    private final static Random generator = new Random();
    
    private int buffer = -1; // compartilhado por todas as threads
    private String hora;
    private boolean temValor = false; //indica se o buffer temValor
    
    //Insere o valor no buffer
    public synchronized void set(int valor, String taskName) throws InterruptedException{
        
        //Enquanto nao houver posicoes vazias, 
        //coloca a thread em estado de Espera
        if ( temValor ){
            hora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
            System.out.println(taskName + "vermelho " + hora);
            
            //wait();
            //Thread.sleep(1000);
            Thread.sleep(generator.nextInt(300));
        } else {
        
        buffer = valor; //insere um valor no buffer
        
        //Indica que o produtor não pode armazenar outro valor ate que
        //o consumidor recupere o valor do buffer.
        temValor = true; 
        
        hora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
        System.out.println(taskName + "verde " + hora);
        
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        //notifyAll(); 
        }
    }
    
    public synchronized int get(String taskName) throws InterruptedException{
    
        if (!temValor){
            hora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
            System.out.println(taskName + "vermelho " + hora);
            //Entra em espera ate que a thread seja notificada
            //Thread.sleep(1000);
            Thread.sleep(generator.nextInt(300));
            //wait();
        } else {
        
        //Consumidor acabou de recuperar valor do buffer.
        //Produtor pode inserir outro valor
        temValor=false;
        
        hora = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
        System.out.println(taskName + "vermelho " + hora);
        //if (buffer<4)
        //    buffer++;
        //else
        //    buffer=1;
        //Informa a todas as outras threads em espera para
        //entrarem no estado Executável
        //notifyAll();
        }
        
        return buffer;
                
    }//fim get
}//fim classe